import cv2 as cv
import numpy as np
import glob
from collections import deque

def find_fields(img, color):
    # eps_min = 70
    # eps_max = 90
    eps_start = 20
    eps_trans = 70
    steps = [
        [-1, 0],
        [1, 0], 
        [0, -1], 
        [0, 1]
    ]
    used = np.zeros([img.shape[0], img.shape[1]])
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            d = abs(img[i][j][0] - color[0]) - (img[i][j][1] - color[1]) + abs(img[i][j][2] - color[2])
            # d = abs(img[i][j][0] - color[1])
            if (used[i][j] == 0 and d < eps_start):
                q = deque()
                q.append([i, j])
                while(q):
                    t = q.popleft()
                    x, y = t[0], t[1]
                    used[x][y] = 1
                    for u in range(0, 4):
                        x1, y1 = x + steps[u][0], y + steps[u][1] 
                        if (x1 >= 0 and x1 < img.shape[0] and y1 >= 0 and y1 < img.shape[1] and used[x1][y1] == 0):
                            d = abs(img[x1][y1][0] - color[0]) - (img[x1][y1][1] - color[1]) + abs(img[x1][y1][2] - color[2])
                            # d = abs(img[x1][y1][1] - color[1])
                            if (d < eps_trans):
                                used[x1][y1] = 1
                                q.append([x1, y1])
                            else:
                                img[x][y] = [0, 0, 255]
    return img


def get_watering_map():
    Photos = []
    for file in glob.glob("*.JPG"):
        Photos.append(file)

    min_height = 700
    photo = cv.imread(Photos[0])
    photo = cv.resize(photo, (int(min_height), int(photo.shape[0] * (min_height / photo.shape[1]))))

    new_img = find_fields(photo, [55, 131, 97])
    cv.imwrite("res.jpg", new_img)
get_watering_map()